let queries = {};

queries.Problem1 = {
  query: `SELECT city, CAST(COUNT(*) AS INTEGER) AS Number_of_Matches 
                            FROM worldcupmatches 
                            GROUP BY city ORDER BY city;`,
  path: "src/public/output/1-number-of-matches-per-city.json",
};

queries.Problem2 = {
  query: `SELECT Team , CAST(COUNT(*) AS INTEGER) AS wins
                            FROM (
                            SELECT hometeamname AS Team 
                            FROM worldcupmatches 
                            WHERE hometeamgoals > awayteamgoals 
                            UNION ALL
                            SELECT awayteamname AS Team 
                            FROM worldcupmatches 
                            WHERE hometeamgoals < awayteamgoals 
                            UNION ALL 
                            SELECT hometeamname AS Team 
                            FROM worldcupmatches 
                            WHERE regexp_replace(winconditions, '\\D*', '', 'g') != '' AND regexp_replace(winconditions, '\\D*', '', 'g')::int/10 > regexp_replace(winconditions, '\\D*', '', 'g')::int%10 
                            UNION ALL 
                            SELECT hometeamname AS Team 
                            FROM worldcupmatches 
                            WHERE regexp_replace(winconditions, '\\D*', '', 'g') != '' AND regexp_replace(winconditions, '\\D*', '', 'g')::int/10 < regexp_replace(winconditions, '\\D*', '', 'g')::int%10 
                            ) AS events 
                            GROUP BY team 
                            ORDER BY team;`,
  path: "src/public/output/2-number-of-matches-won-per-team.json",
};

queries.Problem3 = {
  query: `SELECT teaminitials, CAST(COUNT(*) AS INTEGER) AS RED_Cards_COUNT 
                  FROM worldcupmatches AS matches
                  INNER JOIN worldcupplayers AS players
                  ON matches.matchid = players.matchid
                  WHERE event LIKE '%R%' AND year = '2014'
                  GROUP BY teaminitials
                  ORDER BY COUNT(*);`,
  path: "src/public/output/3-number-of-red-flags-per-team-in-2014.json",
};

queries.Problem4 = {
  query: `SELECT playername , CAST(ROUND(SUM(goals) / SUM(total_matches), 2) AS FLOAT) AS probability
                  FROM (SELECT playername , COUNT(*) AS total_matches, SUM(CASE WHEN event LIKE '%G%' THEN 1 ELSE 0 END) AS goals 
                  FROM worldcupplayers GROUP BY playername) AS matches
                  WHERE total_matches > 5
                  GROUP BY playername
                  ORDER BY probability DESC, SUM(total_matches) DESC
                  LIMIT 10;`,
  path: "src/public/output/4-top-players-with-highest-probability-of-scoring-a-goal.json",
};

module.exports = queries;
