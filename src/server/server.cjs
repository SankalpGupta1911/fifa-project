const fs = require("fs").promises;
const path = require("path");

function server(req, res) {
  const url = req.originalUrl;
  console.log(url);

  if (req.method === "GET") {
    switch (url) {
      case "/":
        fs.readFile(path.resolve("public/index.html"))
          .then((data) => {
            res.status(200);
            res.setHeader("Content-Type", "text/html");
            res.end(data);
          })
          .catch((err) => {
            console.error(err);
            res.status(500);
            res.setHeader("Content-Type", "text/plain");
            res.end("Oops! Something went wrong while fetching the data!");
          });
        break;
      case "/index.cjs":
        // res.status(200);
        // res.setHeader("Content-Type", "text/plain");
        fs.readFile(path.resolve("public/index.cjs"), "utf-8")
          .then((data) => {
            res.send(data);
          })
          .catch((err) => {
            console.error(err);
            res.status(500);
            res.setHeader("Content-Type", "text/plain");
            res.end("Oops! Something went wrong while fetching the data!");
          });
        break;
      case "/style.css":
        fs.readFile(path.resolve("public/style.css"), "utf-8")
          .then((data) => {
            res.send(data);
          })
          .catch((err) => {
            console.error(err);
            res.status(500);
            res.setHeader("Content-Type", "text/plain");
            res.end("Oops! Something went wrong while fetching the data!");
          });
        break;

      case "/1-number-of-matches-per-city":
        fs.readFile(
          path.resolve("./src/public/output/1-number-of-matches-per-city.json"),
          "utf-8"
        )
          .then((data) =>
            prepareData(
              JSON.parse(data),
              "Number of Matches Played Per City",
              "Number of matches held"
            )
          )
          .then((data) => {
            res.send(data);
          })
          .catch((err) => {
            console.error(err);
            res.status(500);
            res.setHeader("Content-Type", "text/plain");
            res.end("Oops! Something went wrong while fetching the data!");
          });
        break;

      case "/2-number-of-matches-won-per-team":
        fs.readFile(
          path.resolve(
            "./src/public/output/2-number-of-matches-won-per-team.json"
          ),
          "utf-8"
        )
          .then((data) =>
            prepareData(
              JSON.parse(data),
              `Number of Matches Won Per Team`,
              `Number of matches won`
            )
          )
          .then((data) => {
            res.send(data);
          })
          .catch((err) => {
            console.error(err);
            res.status(500);
            res.setHeader("Content-Type", "text/plain");
            res.end("Oops! Something went wrong while fetching the data!");
          });
        break;
      case "/3-number-of-red-flags-per-team-in-2014":
        fs.readFile(
          path.resolve(
            "./src/public/output/3-number-of-red-flags-per-team-in-2014.json"
          ),
          "utf-8"
        )
          .then((data) =>
            prepareData(
              JSON.parse(data),
              `Number of Red Flags Recieved Per Team in 2014`,
              "Number of Red Cards"
            )
          )
          .then((data) => {
            res.send(data);
          })
          .catch((err) => {
            console.error(err);
            res.status(500);
            res.setHeader("Content-Type", "text/plain");
            res.end("Oops! Something went wrong while fetching the data!");
          });
        break;
      case "/4-top-players-with-highest-probability-of-scoring-a-goal":
        fs.readFile(
          path.resolve(
            "./src/public/output/4-top-players-with-highest-probability-of-scoring-a-goal.json"
          ),
          "utf-8"
        )
          .then((data) =>
            prepareData(
              JSON.parse(data),
              "Top 10 Players With Highest Probability to Score in a Match",
              "Probability to Goal"
            )
          )
          .then((data) => {
            res.send(data);
          })
          .catch((err) => {
            console.error(err);
            res.status(500);
            res.setHeader("Content-Type", "text/plain");
            res.end("Oops! Something went wrong while fetching the data!");
          });
        break;
      default:
        res.status(500);
        res.setHeader("Content-Type", "text/plain");
        res.end("Oops! Something went wrong while fetching the data!");
    }
  } else {
    res.status(500);
    res.setHeader("Content-Type", "text/plain");
    res.end("Oops! Something went wrong while fetching the data!");
  }
}

function prepareData(tempData, title, pointer) {
  const keys = Object.keys(tempData[0]);
  let res = tempData.reduce((acc, iter) => {
    let temp = [];
    temp.push(iter[keys[0]]);
    temp.push(iter[keys[1]]);
    acc.push(temp);
    return acc;
  }, []);
  return [res, keys, title, pointer];
}

module.exports = server;
