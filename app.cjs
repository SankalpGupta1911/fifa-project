const express = require("express");
const path = require("path");
const port = process.env.PORT || 4000;
const host = "localhost";
const fs = require("fs").promises;
const server = require("./src/server/server.cjs");

const app = express();

app.use(server);

app.listen(port, host, (err) => {
  if (err) {
    console.error(err);
  } else {
    console.log(`Server running at: http://${host}:${port} `);
  }
});
