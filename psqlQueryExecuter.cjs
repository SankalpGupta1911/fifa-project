const path = require("path");
const fs = require("fs").promises;
const { Client } = require("pg");
const queries = require("./src/server/queries.cjs");

runQueryAtPath(queries.Problem1.query, queries.Problem1.path);
runQueryAtPath(queries.Problem2.query, queries.Problem2.path);
runQueryAtPath(queries.Problem3.query, queries.Problem3.path);
runQueryAtPath(queries.Problem4.query, queries.Problem4.path);

function runQueryAtPath(query, resultPath) {
  const client = new Client({
    host: "localhost",
    user: "postgres",
    port: 5432,
    password: "1234567890",
    database: "postgres",
  });

  const server = client.connect();
  server
    .then(() => client.query(query))
    .then((res) => {
      fs.writeFile(
        path.join(__dirname, resultPath),
        JSON.stringify(res.rows),
        "utf-8"
      );
    })
    .then(() => client.end())
    .catch((err) => console.error(err));
}
