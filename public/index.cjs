function buttonAction(action) {
  fetch(`http://localhost:4000/${action}`)
    .then((data) => data.json())
    .then((result) =>
      callChart(
        result[0],
        result[1][0].replaceAll("_", " "),
        result[1][1].replaceAll("_", " "),
        result[2],
        result[3]
      )
    )
    .catch((err) => {
      console.error(err);
    });
}

function callChart(actionData, xAxis, yAxis, actionTitle, pointer) {
  Highcharts.chart("container", {
    chart: {
      type: "column",
    },
    title: {
      text: actionTitle,
    },
    subtitle: {
      text: 'Source: <a href="https://www.kaggle.com/datasets/abecklas/fifa-world-cup?" target="_blank">FIFA World Cup Review</a>',
    },
    xAxis: {
      type: "category",
      title: {
        text: xAxis,
      },
      labels: {
        rotation: -45,
        style: {
          fontSize: "13px",
          fontFamily: "Verdana, sans-serif",
        },
      },
    },
    yAxis: {
      min: 0,
      title: {
        text: yAxis,
      },
    },
    legend: {
      enabled: false,
    },
    tooltip: {
      pointFormat: `${pointer}: <b>{point.y:.f}</b>`,
    },
    series: [
      {
        name: "Population",
        data: actionData,
        dataLabels: {
          enabled: true,
          rotation: -90,
          color: "#FFFFFF",
          align: "right",
          format: "{point.y}",
          y: 10, // 10 pixels down from the top
          style: {
            fontSize: "13px",
            fontFamily: "Verdana, sans-serif",
          },
        },
      },
    ],
  });
}
